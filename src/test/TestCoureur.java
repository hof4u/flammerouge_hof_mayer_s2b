package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.*;

import org.junit.*;

import Jeu.*;

public class TestCoureur {

	@Test
	public void testdepOk() throws IOException{
		Jeu jeu = new Jeu(0,1);
		ArrayList<Coureur> c = new ArrayList<Coureur>(); 
		Coureur cour1 = new Coureur(1,"R",jeu);
		Coureur cour2 = new Coureur(1,"S",jeu);
		cour1.setAvance(2);
		cour2.setAvance(3);
		c.add(cour1);
		c.add(cour2);
		jeu.setCour(c);
		boolean dep = jeu.getCour().get(0).depOk(1,0);
		assertEquals("le deplacement ne doit pas se faire:",false,dep);
		dep = jeu.getCour().get(0).depOk(2,1);
		assertEquals("le deplacement doit se faire:",true,dep);
		
	}
}
