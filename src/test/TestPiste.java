package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.*;

import org.junit.*;

import Jeu.*;

public class TestPiste {
	
	@Test
	public void testgetTuile() throws IOException{
		
		Jeu jeu =new Jeu(0,1);
		ArrayList<Coureur> c = new ArrayList<Coureur>(); 
		Coureur cour1 = new Coureur(1,"R",jeu);
		Coureur cour2 = new Coureur(1,"S",jeu);
		cour1.setAvance(2);
		cour2.setAvance(3);
		c.add(cour1);
		c.add(cour2);
		jeu.setCour(c);
		Piste p = jeu.getP();
		String t=p.getTuile(cour1);
		assertEquals("la tuile doit etre celle du depart","D",t);
	}
	
	@Test
	public void testTailleTuile() throws IOException{
		
		Jeu jeu =new Jeu(0,1);
		ArrayList<Coureur> c = new ArrayList<Coureur>(); 
		Coureur cour1 = new Coureur(1,"R",jeu);
		Coureur cour2 = new Coureur(1,"S",jeu);
		cour1.setAvance(2);
		cour2.setAvance(3);
		c.add(cour1);
		c.add(cour2);
		jeu.setCour(c);
		Piste p = jeu.getP();
		int t =p.tailleTuile();
		assertEquals("la tuile doit etre celle du depart",52,t);
	}
	
}
