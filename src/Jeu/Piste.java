/**
 * Classe appartenant au package Jeu
 */
package Jeu;

/**
 * Importation de java.io
 */
import java.io.*;
import java.util.*;

import tuiles.*;

/**
 * @author Mayer Theo, Hof Lucien
 */

/**
 * Classe Piste, permettant de creer une piste
 * Cette classe implemente Serializable
 */
public class Piste implements Serializable{
	
	//ATTRIBUT
	/**
	 * Attribut ArrayList<Tuile> tuiles, creant une liste de tuile
	 */
	private ArrayList<Tuile> tuiles;
	
	//CONSTRUCTEUR
	/**
	 * Constructeur de la classe Piste
	 * 
	 * @param p
	 * @throws IOException
	 */
	public Piste(int p) throws IOException{
		
		this.tuiles=new ArrayList<Tuile>();
		String ligne;
		try{
			BufferedReader br = new BufferedReader(new FileReader("Circuits.txt"));
			for(int i=0;i<p;i++){
				br.readLine();
			}
			ligne=br.readLine();
			String[] t;
			t = ligne.split(" ");
			for(int i=0;i<t.length;i++){
				if(t[i].equals("D")){
					this.tuiles.add(new Depart(0));
				}
				if(t[i].equals("A")){
					this.tuiles.add(new Arrive(0));
				}
				if(t[i].equals("L")){
					this.tuiles.add(new LigneDroite(0));
				}
				if(t[i].equals("SG")){
					this.tuiles.add(new VirageSerre(0,false));
				}
				if(t[i].equals("SD")){
					this.tuiles.add(new VirageSerre(0,true));
				}
				if(t[i].equals("LG")){
					this.tuiles.add(new VirageLarge(0,false));
				}
				if(t[i].equals("LD")){
					this.tuiles.add(new VirageLarge(0,true));
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	//METHODES
	/**
	 * Methode String getTuile, permettant d obtenir la tuile ou le coureur est situe
	 * 
	 * @param c, coureur
	 * 
	 * @return this.tuiles.get(j).toString()
	 */
	public String getTuile(Coureur c){
		int i=0;
		int j=-1;
		while(c.getAvance()>=i){
			j++;
			i=i+this.tuiles.get(j).getTaille();
		}
		return this.tuiles.get(j).toString();
	}
	
	/**
	 * Methode int tailleTuile, permettant d obtenir la taille de la tuile
	 * 
	 * @return res
	 */
	public int tailleTuile(){
		int res=0;
		for(int i=0;i<this.tuiles.size();i++){
			res=res+this.tuiles.get(i).getTaille();
		}
		return res;
	}

	
	
	/*
	 * PISTE
	 * 
	 * La Classicissima : D SG L LD LG L LD SG L SG SG LD L L LD SD SD L LG A
	 * Milano : D L L SG SG L LD LG L LG L LG SG L LD LG LD LG LD L A
	 * Ruenue Corso Paseo : D L L L SG L SG LG LG LD LD L L L SD SD LG LD LD LG A 
	 * La Haute Montagne : D L L L SG L SG LG LG SD L LD LG LD L SD SD LG LD L A 
	 * Le Col du Ballon : D L L LD SG L SG LG LG LD L L SD SG SD SD L L LD LG A 
	 * Ronde Van Wevelgem : D L L L SG L LG LG LG SD L L SG SD LD SD L LD LD LG A
	 */
	
}
