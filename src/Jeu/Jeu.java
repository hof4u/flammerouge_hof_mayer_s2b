/**
 * Classe appartenant au package Jeu
 */
package Jeu;

/**
 * Importation de java.io
 */
import java.io.*;
import java.util.*;

import Carte.*;

/**
 * @author Mayer Theo, Hof Lucien
 */

/**
 * Classe Jeu, permettant d initialiser le jeu, de gerer le jeu, il sera lancer dans la classe Main
 * Cette classe implemente Serializable
 *
 */
public class Jeu implements Serializable{
	
	//ATTRIBUTS
	/**
	 * Attribut boolean sauvegarde
	 */
	private boolean sauvegarde;
	/**
	 * Attribut boolean fini
	 */
	private boolean fini;
	/**
	 * Attribut Piste p
	 */
	private Piste p;
	/**
	 * Attribut ArrayList<CarteFatigue> rouleurFatigue
	 */
	private ArrayList<CarteFatigue> rouleurFatigue;
	/**
	 * Attribut ArrayList<CarteFatigue> sprinterFatigue
	 */
	private ArrayList<CarteFatigue> sprinterFatigue;
	/**
	 * Attribut ArrayList<Plateau> plat
	 */
	private ArrayList<Plateau> plat;
	/**
	 * Attribut ArrayList<Coureur> cour
	 */
	private ArrayList<Coureur> cour;
	/**
	 * Attribut int nbJoueur
	 */
	private int nbJoueur;
	/**
	 * Attribut ArrayList<String> nom
	 */
	private ArrayList<String> nom;
	
	
	//CONSTRUCTEURS
	
	/**
	 * Premier constructeur de la classe Jeu
	 * Ce constructeur prend en parametre une piste et un nombre de joueur, il sera donc initialise avec ses parametres et se sera un nouveau jeu qui sera lancer
	 * 
	 * @param piste, choix de la piste
	 * @param nbJ, nombre de joueur
	 * @throws IOException
	 */
	public Jeu(int piste,int nbJ) throws IOException{
		this.fini=false;
		this.p = new Piste(piste);
		this.plat = new ArrayList<Plateau>();
		this.rouleurFatigue = new ArrayList<CarteFatigue>();
		this.sprinterFatigue = new ArrayList<CarteFatigue>();
		this.cour=new ArrayList<Coureur>();
		this.nbJoueur=nbJ;
		this.nom=new ArrayList<String>();
		this.sauvegarde=false;
		Scanner sc = new Scanner(System.in);
		for(int i=0;i<this.nbJoueur;i++){
			System.out.println("Entrez votre nom:");
			this.nom.add(sc.next());
		}
		System.out.println(this.nom.get(0)+" sera le joueur 1 en rouge");
		if(this.nom.size()>=2)
			System.out.println(this.nom.get(1)+" sera le joueur 2 en vert");
		if(this.nom.size()>=3)
			System.out.println(this.nom.get(2)+" sera le joueur 3 en bleu");
		if(this.nom.size()>=4)
			System.out.println(this.nom.get(3)+" sera le joueur 4 en noir");
		
		for(int i=1;i<=nbJ;i++){
			this.cour.add(new Coureur(i,"R",this));
			this.cour.add(new Coureur(i,"S",this));
			plat.add(new Plateau(i));
			plat.get(i-1).MelangerCarte();
		}

		for(int i=0;i<30;i++){
			rouleurFatigue.add(new Rouleur());
			sprinterFatigue.add(new Sprinter());
		}
		
	}
	
	/**
	 * Deuxieme constructeur de la classe Jeu, lui pernet de charger un jeu rentre en parametre quand un jeu a ete sauvegarde
	 * 
	 * @param j, jeu prealablement sauvegarde
	 */
	public Jeu(Jeu j){
		this.fini=j.fini;
		this.p=j.p;
		this.rouleurFatigue=j.rouleurFatigue;
		this.sprinterFatigue=j.sprinterFatigue;
		this.plat=j.plat;
		this.cour=j.cour;
		this.nbJoueur=j.nbJoueur;
		this.sauvegarde=true;
	}
	
	//METHODES
	
	/**
	 * Methode void sauvegarder, qui sert donc a pouvoir sauvegarder la partie en cours
	 * 
	 * @throws IOException
	 */
	public void sauvegarder() throws IOException{
		try{
			ObjectOutputStream oo = new ObjectOutputStream(new FileOutputStream("save.txt"));
			oo.writeObject(this);
			oo.close();
		}catch(IOException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Methode Jeu charger, qui permet de charger une partie qui a ete sauvegarder
	 * 
	 * @return jeu
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public Jeu charger() throws IOException, ClassNotFoundException{
		Jeu jeu= null;
		try{
			ObjectInputStream oi = new ObjectInputStream(new FileInputStream("save.txt"));
			jeu=((Jeu)(oi.readObject()));
			oi.close();
		
		}catch(IOException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		return jeu;
	}
	
	/**
	 * Methode Coureur getCour, permet de retourner le coureur a une position donnee
	 * 
	 * @param int i, variable de position
	 * @return this.cour.get(i)
	 */
	public Coureur getCour(int i){
		return this.cour.get(i);
	}
	
	/**
	 * Methode int getNbCour, permettant de retourner la taille de l ArrayList de coureur
	 * 
	 * @return this.cour.size()
	 */
	public int getNbCour(){
		return this.cour.size();
	}
	
	/**
	 * Methode void demarrerjeu, qui permet donc de pouvoir demarrer le jeu
	 * @throws IOException 
	 */
	public void demarerjeu() throws IOException{
		
		boolean tour=false;
		Scanner sc = new Scanner(System.in);

		//placement des joueur au d�but de la partie
		if(!this.sauvegarde){
		int coorX=0;
		int droite=0;
		String[][] affiche ={{"-"," ","-"," ","-"," ","-"," ","-"},{"-"," ","-"," ","-"," ","-"," ","-"}};
			for(int i=1;i<this.cour.size()+1;i++){
				if(i==1)
					System.out.println("Coureurs du premier joueur rouge");
				if(i==3)
					System.out.println("Coureurs du deuxieme joueur vert");
				if(i==5)
					System.out.println("Coureurs du troisieme joueur bleu");
				if(i==7)
					System.out.println("Coureurs du quatrieme joueur noir");
				if(i%2!=0){
					System.out.println("Rouleur du joueur "+ (i+1)/2 +":");
					System.out.println("case n� (entre 0 et 4):");
					coorX= sc.nextInt();
					//test case autoris�
					while(coorX <0 || coorX >4){
						System.out.println("la valeur doit �tre comprise entre 0 et 4");
						System.out.println("case n� (entre 0 et 4):");
						coorX= sc.nextInt();
					}
					System.out.println("droite (1) ou gauche(0) :");
					droite= sc.nextInt();
					//test droite ou gauche
					while(droite <0 || droite >1){
						System.out.println("la valeur doit �tre comprise entre 0 et 1");
						System.out.println("droite (1) ou gauche(0) :");
						droite= sc.nextInt();
					}
					//test case occup�
					while(!this.cour.get(i-1).depOk(coorX , droite)){
						System.out.println("Erreur case deja occup�");
						System.out.println("case n� (entre 0 et 4):");
						coorX= sc.nextInt();
						//test case autoris�
						while(coorX <0 || coorX >4){
							System.out.println("la valeur doit �tre comprise entre 0 et 4");
							System.out.println("case n� (entre 0 et 4):");
							coorX= sc.nextInt();
						}
						System.out.println("droite (1) ou gauche(0) :");
						droite= sc.nextInt();
						//test droite ou gauche
						while(droite <0 || droite >1){
							System.out.println("la valeur doit �tre comprise entre 0 et 1");
							System.out.println("droite (1) ou gauche(0) :");
							droite= sc.nextInt();
						}
					}
				}else{
					System.out.println("Sprinter du joueur "+ i/2 +":");
					System.out.println("case n� (entre 0 et 4):");
					coorX= sc.nextInt();
					//test case autoris�
					while(coorX <0 || coorX >4){
						System.out.println("la valeur doit �tre comprise entre 0 et 4");
						System.out.println("case n� (entre 0 et 4):");
						coorX= sc.nextInt();
					}
					System.out.println("droite (1) ou gauche(0) :");
					droite= sc.nextInt();
					//test droite ou gauche
					while(droite <0 || droite >1){
						System.out.println("la valeur doit �tre comprise entre 0 et 1");
						System.out.println("droite (1) ou gauche(0) :");
						droite= sc.nextInt();
					}
					//test case occup�
					while(!this.cour.get(i-1).depOk(coorX , droite)){
						System.out.println("Erreur case deja occup�");
						System.out.println("case n� (entre 0 et 4):");
						coorX= sc.nextInt();
						//test case autoris�
						while(coorX <0 || coorX >4){
							System.out.println("la valeur doit �tre comprise entre 0 et 4");
							System.out.println("case n� (entre 0 et 4):");
							coorX= sc.nextInt();
						}
						System.out.println("droite (1) ou gauche(0) :");
						droite= sc.nextInt();
						//test droite ou gauche
						while(droite <0 || droite >1){
							System.out.println("la valeur doit �tre comprise entre 0 et 1");
							System.out.println("droite (1) ou gauche(0) :");
							droite= sc.nextInt();
						}
					}
			}
				this.cour.get(i-1).setAvance(coorX);
				this.cour.get(i-1).setDroite(droite);
				
				affiche[droite][coorX*2]=this.cour.get(i-1).toString();
				System.out.println("__________");
				for(int b=0; b < affiche[0].length ; b++){
					System.out.print(affiche[0][b]);
				}
				System.out.println("");
				System.out.println("__________        --------->");
				for(int b=0; b < affiche[1].length ; b++){
					System.out.print(affiche[1][b]);
				}
				System.out.println("");
				System.out.println("__________");
		}
		}
		
			ArrayList<String> perso = new ArrayList<String>();
			ArrayList<CarteCycliste> cartePiocher = new ArrayList<CarteCycliste>();
			ArrayList<CarteCycliste> carteChoisT1 = new ArrayList<CarteCycliste>();
			ArrayList<CarteCycliste> carteChoisT2 = new ArrayList<CarteCycliste>();
			int choix;
			
		//d�but jeu	
		while(this.fini!=true){
			
			//tri du plus devant au plus derriere
			Collections.sort(this.cour);
			boolean fatigue=false;
			
			//premi�re pioche 
			for( int i=0 ; i<this.nbJoueur ; i++){
				if(tour && this.cour.get(0).getColor()==i+1 && !fatigue){
					fatigue=true;
					perso.add(this.cour.get(0).getType());
					
				}else{
					System.out.println("Joueur "+ (i+1) +" choisi entre Sprinter (S) ou rouleur (R):");
					perso.add(sc.next());
					System.out.println("Joueur "+ (i+1) +" pioche 4 carte et en choisie 1:");
					if(perso.get(i).equals("S")){
						for(int j=0; j<4;j++){
							cartePiocher.add(this.plat.get(i).piocheSprinter());
							System.out.println(cartePiocher.get(j).toString());
						}
					}else{
						for(int j=0; j<4;j++){
							cartePiocher.add(this.plat.get(i).piocheRouleur());
							System.out.println(cartePiocher.get(j).toString());
						}
					}
					System.out.println("Choisir une carte parmi celle affich� (choisissez entre 1 et 4):");
					choix=sc.nextInt();
					System.out.println("vous avez choisi la carte :"+cartePiocher.get(choix-1).toString());
					System.out.println("");
					carteChoisT1.add(cartePiocher.get(choix-1));
					cartePiocher.remove(choix-1);
					for(int j=0 ; j<cartePiocher.size() ; j++){
						if(perso.get(i).equals("R")){
							plat.get(i).defausseRouleur((CarteRouleur)cartePiocher.get(j));
						}else{
							plat.get(i).defausseSprinter((CarteSprinter)cartePiocher.get(j));
						}
					}
					cartePiocher.clear();	
				}
			}
			
			//deuxi�me pioche
			for( int i=0 ; i<this.nbJoueur ; i++){
				if(perso.get(i).equals("S")){
					System.out.println("Le joueur "+ (i+1) +" choisi une carte rouleur pour son autre coureur :");
					for(int j=0; j<4;j++){
						cartePiocher.add(this.plat.get(i).piocheRouleur());
						System.out.println(cartePiocher.get(j).toString());
					}
				}else{
					System.out.println("Le joueur "+ (i+1) +" choisi une carte sprinter pour son autre coureur :");
					for(int j=0; j<4;j++){
						cartePiocher.add(this.plat.get(i).piocheSprinter());
						System.out.println(cartePiocher.get(j).toString());
					}
				}
				choix=sc.nextInt();
				System.out.println("vous avez choisi la carte :"+cartePiocher.get(choix-1).toString());
				System.out.println("");
				carteChoisT2.add(cartePiocher.get(choix-1));
				for(int j=0 ; j<cartePiocher.size() ; j++){
					if(perso.get(i).equals("S")){
						plat.get(i).defausseRouleur((CarteRouleur)cartePiocher.get(j));
					}else{
						plat.get(i).defausseSprinter((CarteSprinter)cartePiocher.get(j));
					}
				}
				cartePiocher.clear();
				//clear de console
			}
			
			
			
			//d�placement coureur
			boolean dep=false;
			for(int i=0;i<this.cour.size();i++){
				if(i==0 && tour){
					this.cour.get(0).depOk(2, 1);
					if(this.cour.get(0).getType()=="S"){
						this.sprinterFatigue.remove(0);
					}else{
						this.rouleurFatigue.remove(0);
					}
				}else{
				switch(this.cour.get(i).getColor()){
				case 1:
					if(this.cour.get(i).getType().equals(perso.get(0))){
						dep=this.cour.get(i).depOk(carteChoisT1.get(0).getValeur(), 1);
						if(dep==false)
							this.cour.get(i).depOk(carteChoisT1.get(0).getValeur(), 0);	
						else
							dep=false;
					}else{
						dep=this.cour.get(i).depOk(carteChoisT2.get(0).getValeur(), 1);
						if(dep==false)
							this.cour.get(i).depOk(carteChoisT2.get(0).getValeur(), 0);	
						else
							dep=false;
					}
					break;
				case 2:
					if(this.cour.get(i).getType().equals(perso.get(1))){
						dep=this.cour.get(i).depOk(carteChoisT1.get(1).getValeur(), 1);
						if(dep==false)
							this.cour.get(i).depOk(carteChoisT1.get(1).getValeur(), 0);
						else
							dep=false;
					}else{
						dep=this.cour.get(i).depOk(carteChoisT2.get(1).getValeur(), 1);
						if(dep==false)
							this.cour.get(i).depOk(carteChoisT2.get(1).getValeur(), 0);	
						else
							dep=false;
					}
					break;
				case 3:
					if(this.cour.get(i).getType().equals(perso.get(2))){
						dep=this.cour.get(i).depOk(carteChoisT1.get(2).getValeur(), 1);
						if(dep==false)
							this.cour.get(i).depOk(carteChoisT1.get(2).getValeur(), 0);	
						else
							dep=false;
					}else{
						dep=this.cour.get(i).depOk(carteChoisT2.get(2).getValeur(), 1);
						if(dep==false)
							this.cour.get(i).depOk(carteChoisT2.get(2).getValeur(), 0);	
						else
							dep=false;
					}
					break;
				case 4:
					if(this.cour.get(i).getType().equals(perso.get(3))){
						dep=this.cour.get(i).depOk(carteChoisT1.get(3).getValeur(), 1);
						if(dep==false)
							this.cour.get(i).depOk(carteChoisT1.get(3).getValeur(), 0);	
						else
							dep=false;
					}else{
						dep=this.cour.get(i).depOk(carteChoisT2.get(3).getValeur(), 1);
						if(dep==false)
							this.cour.get(i).depOk(carteChoisT2.get(3).getValeur(), 0);	
						else
							dep=false;
					}
					break;
				}
				}
			}
			
			carteChoisT1.clear();
			carteChoisT2.clear();
		
			
			//test aspiration
			//tri du plus derriere au plus devant
			
			Collections.sort(this.cour);
			Collections.reverse(cour);
			
			for(int i=0 ;i<this.cour.size();i++)
				System.out.println(this.cour.get(i).toString());
			
			boolean aspi=false;
			boolean depla=false;
			while (!aspi){
				
				aspi=true;
				for(int i=0 ;i<this.cour.size();i++){
					for(int j=0 ;j<this.cour.size();j++){
						if((this.cour.get(i).getAvance()+2)==(this.cour.get(j).getAvance())){
							System.out.println(this.cour.get(i).toString());
							depla=this.cour.get(i).depOk(1, this.cour.get(i).getDroite());
							if(depla)
								aspi=false;
						}
					}
				}
				
				
			}
			
			
			
			
			//affichage
		
			//creation du tableau d'affichage
			String[][] affich = new String[3][this.p.tailleTuile()*2];
			for(int i = 0 ; i<3 ; i++){
				for(int j = 0 ; j<this.p.tailleTuile()*2 ; j++){
					if(i!=2){
						if(j%2==1){
							affich[i][j]="  ";
						}else{
							affich[i][j]="|";
						}
					}
					if(i==2){
						if(j<20){
							if(j%2==1){
							affich[i][j]=(j-1)/2+" ";
							}else{
								affich[i][j]=" ";
							}
						}else{
							if(j%2==1){
								affich[i][j]=""+(j-1)/2;
								}else{
									affich[i][j]=" ";
								}
								if(j==(this.p.tailleTuile()*2)-1)
									affich[i][j]="Arriv�e";
						}
					}
				}
			}
			
			//gestion joueur
			for(int i = 0 ; i<this.cour.size();i++){
				if(this.cour.get(i).getAvance()>this.p.tailleTuile()){
					this.cour.get(i).setAvance(this.p.tailleTuile());
					affich[this.cour.get(i).getDroite()][this.cour.get(i).getAvance()*2]=this.cour.get(i).toString();
				}else
					affich[this.cour.get(i).getDroite()][this.cour.get(i).getAvance()*2+1]=this.cour.get(i).toString();
			}
			
			
			
			
			//affichage circuit
			System.out.println("________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________");
			for(int i=0;i<this.p.tailleTuile()*2;i++)
				System.out.print(affich[0][i]);
			System.out.println("");
			System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			for(int i=0;i<this.p.tailleTuile()*2;i++)
				System.out.print(affich[1][i]);
			System.out.println("");
			System.out.println("________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________");
			for(int i=0;i<this.p.tailleTuile()*2;i++)
				System.out.print(affich[2][i]);
			System.out.println("");
			
			
			
			
			
			System.out.println("fin du tour");
			
			perso.clear();
			
			//test fin du jeu
			int gagnant=0;
			for(int i=0 ; i < this.cour.size() ; i++)
				if(p.getTuile(this.cour.get(i)).equals("A")){
					this.fini=true;
					gagnant =i;
				}
			tour = true;
			
			//affichage vainqueur
			if(fini==true){
				System.out.println("Bravo le coureur "+this.cour.get(gagnant).toString()+" a fini la course !");
				if(gagnant%2==1){
					System.out.println(this.nom.get((gagnant-1)/2)+" a gagn� la partie");
				}else{
					System.out.println(this.nom.get(gagnant/2)+" a gagn� la partie");
				}
				
			}
			
			System.out.println("Voulez vous sauvegarder votre partie pour la continuez plus tard ? oui(o) ou non(n)");
			String sauv = sc.next();
			if(sauv.equals("o")){
				this.sauvegarder();	
				fini=true;
			}
		}
	}

	/**
	 * Methode Piste getP, permettant d obtenir la piste
	 * 
	 * @return p
	 */
	public Piste getP() {
		return p;
	}
	
	/**
	 * Methode void setP, permettant de setter une piste a la piste en attribut
	 * 
	 * @param Piste p
	 */
	public void setP(Piste p) {
		this.p = p;
	}
	
	/**
	 * Methode ArrayList<CarteFatigue> getRouleurFatigue, permettant d obtenir la liste carte fatigue rouleur
	 * 
	 * @return rouleurFatigue
	 */
	public ArrayList<CarteFatigue> getRouleurFatigue() {
		return rouleurFatigue;
	}
	
	/**
	 * Methode void setRouleurFatigue, permettant de setter une liste de carte fatigue a rouleurFatigue
	 * 
	 * @param rouleurFatigue
	 */
	public void setRouleurFatigue(ArrayList<CarteFatigue> rouleurFatigue) {
		this.rouleurFatigue = rouleurFatigue;
	}
	
	/**
	 * Methode ArrayList<CarteFatigue> getRouleurFatigue, permettant d obtenir la liste carte fatigue sprinter
	 * 
	 * @return sprinterFatigue
	 */
	public ArrayList<CarteFatigue> getSprinterFatigue() {
		return sprinterFatigue;
	}
	
	/**
	 * Methode void setSprinterFatigue, permettant de setter une liste de carte fatigue a sprinterFatigue
	 * 
	 * @param sprinterFatigue
	 */
	public void setSprinterFatigue(ArrayList<CarteFatigue> sprinterFatigue) {
		this.sprinterFatigue = sprinterFatigue;
	}
	
	/**
	 * Methode ArrayList<Plateau> getPlat, permettant d obtenir le plateau
	 * 
	 * @return plat
	 */
	public ArrayList<Plateau> getPlat() {
		return plat;
	}
	
	/**
	 * Methode void setPlat, permettant de setter une liste de plateau a plat
	 * 
	 * @param plat
	 */
	public void setPlat(ArrayList<Plateau> plat) {
		this.plat = plat;
	}
	
	/**
	 * Methode ArrayList<Coureur> getCour, permettant d obtenir les coureurs
	 * 
	 * @return cour
	 */
	public ArrayList<Coureur> getCour() {
		return cour;
	}
	
	/**
	 * Methode void setCour, permettant de setter une liste de coureur a cour
	 * 
	 * @param cour
	 */
	public void setCour(ArrayList<Coureur> cour) {
		this.cour = cour;
	}
	
	/**
	 * Methode int getNbJoueur, permettant d obtenir le nombre de joueur
	 * 
	 * @return nbJoueur
	 */
	public int getNbJoueur() {
		return nbJoueur;
	}
	
	/**
	 * Methode void setNbJoueur, permettant de setter un nombre de joueur a nbJoueur
	 * 
	 * @param nbJoueur
	 */
	public void setNbJoueur(int nbJoueur) {
		this.nbJoueur = nbJoueur;
	}
	
	/**
	 * Methode ArrayList<String> getNom, permettant d obtenir le nom
	 * 
	 * @return nom
	 */
	public ArrayList<String> getNom() {
		return nom;
	}
	
	/**
	 * Methode void setNom, permettant de setter une liste de nom a nom
	 * 
	 * @param nom
	 */
	public void setNom(ArrayList<String> nom) {
		this.nom = nom;
	}
} 
			
