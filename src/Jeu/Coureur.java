/**
 * Classe appartenant au package Jeu
 */
package Jeu;

import java.io.Serializable;
/**
 * Importation de java.util
 */
import java.util.*;

/**
 * @author Mayer Theo, Hof Lucien
 */

/**
 * Classe Coureur qui permet de creer un coureur
 * Elle implemente Comparator<Coureur> et Comparable<Coureur> et Serializable
 */
public class Coureur implements Comparator<Coureur>, Comparable<Coureur>, Serializable{
	
	//ATTRIBUTS
	/**
	 * Attribut int couleur, permettant de donner une couleur au coureur, 1=rouge , 2=verte , 3=bleue , 4=noire
	 */
	private int couleur;
	/**
	* Attribut String type, permettant de donner un type au coureur, "R"=Rouleur , "S"=Sprinter
	*/
	private String type;
	/**
	 * Attribut int droite, permettant de savoir si le coureur est sur la droite ou sur la gauche de la piste, 1=droite , 0=gauche
	 */
	private int droite;
	/**
	 * Attribut inr avance, permettant d avoir l avance d un coureur
	 */
	private int avance;
	/**
	 * Attribut Jeu jeu, permettant d acceder a un autre coureur
	 */
	private Jeu jeu;
	
	//CONSTRUCTEUR
	/**
	 * Constructeur de la classe Coureur, permmetant de creer un coureur suivant les parametres
	 * 
	 * @param c, couleur du coureur
	 * @param t, type du coureur
	 * @param j, acces du coureur
	 */
	public Coureur(int c, String t, Jeu j){
		this.couleur=c;
		this.type=t;
		this.jeu=j;
		this.avance=0;
		this.droite=0;
	}
	
	//METHODES
	/**
	 * Methode int getColor, permmetant d obtenir la couleur du coureur
	 * 
	 * @return this.couleur
	 */
	public int getColor(){
		return this.couleur;
	}
	
	/**
	 * Methode String getType, permmetant d obtenir le type du coureur
	 * 
	 * @return this.type
	 */
	public String getType(){
		return this.type;
	}
	
	/**
	 * Methode void setAvance, permettant de set une avance
	 * 
	 * @param a
	 */
	public void setAvance(int a){
		this.avance=a;
	}
	
	/**
	 * Methode int getAvance, permmetant d obtenir l avance du coureur
	 * 
	 * @return this.avance
	 */
	public int getAvance(){
		return this.avance;
	}
	
	/**
	 * Methode void setDroite, permettant de set une position droite ou gauche
	 * 
	 * @param d
	 */
	public void setDroite(int d){
		this.droite=d;
	}
	
	/**
	 * Methode int getDroite, permettant d obtenir la position du coureur sur la piste
	 * 
	 * @return this.droite
	 */
	public int getDroite(){
		return this.droite;
	}
	
	/**
	 * Methode boolean depOk, permettant de savoir si le depacement est faisable
	 * 
	 * @param av, avance
	 * @param dro, position
	 * 
	 * @return res
	 */
	public boolean depOk(int av, int dro){
		boolean res=true;
		for(int i =0 ;i<this.jeu.getNbCour();i++){
			if(this.jeu.getCour(i).getAvance()==this.avance+av && this.jeu.getCour(i).getDroite()==dro){
				res=false;
		}
		}if(res==true){
			this.avance=this.avance+av;
			this.droite=dro;
		}
		return res;
		
	}
	
	/**
	 * Methode String toString, permettant d obtenir le type et la couleur du coureur
	 * 
	 * @return ""+this.type+""+coul
	 */
	public String toString(){
		String coul = "";
		switch(this.couleur){
		case 0:
			break;
		case 1:
			coul="R";
			break;
		case 2:
			coul="V";
			break;
		case 3:
			coul="B";
			break;
		case 4:
			coul="N";
			break;
		}
		return ""+this.type+""+coul;
	}

	@Override
	/**
	 * Methode int compare, permettant de comparer deux coureurs
	 * 
	 * @param c1, premier coureur
	 * @param c2, deuxieme coureur
	 * 
	 * @return c2.droite-c1.droite
	 */
	public int compare(Coureur c1, Coureur c2) {
		// TODO Auto-generated method stub
		return c1.droite-c2.droite;
	}

	@Override
	/**
	 * Methode inr compareTo, permettant de comparer le coureur en parametre a un coureur de base
	 * 
	 * @param c
	 * 
	 * @return (c.avance-this.avance)
	 */
	public int compareTo(Coureur c) {
		// TODO Auto-generated method stub
		return (c.avance-this.avance);
	}
}
	