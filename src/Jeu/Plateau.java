/**
 * Classe appartenant au package Jeu
 */
package Jeu;

import java.io.Serializable;
/**
 * Importation de java.util
 */
import java.util.*;

import Carte.*;

/**
 * @author Mayer Theo, Hof Lucien
 */

/**
 * Classe Plateau, permettant de creer le plateau de jeu 
 * Cette classe implemente Serializable
 */
public class Plateau implements Serializable{
	
	//ATTRIBUTS
	/**
	 * Attribut int couleur, couleur du coureur, 1=rouge , 2=vert , 3=bleu , 4=noir
	 */
	private int couleur;
	/**
	 * Attribut ArrayList<CarteCycliste> cRoul, liste de carte rouleur
	 */
	private ArrayList<CarteCycliste> cRoul;
	/**
	 * Attribut ArrayList<CarteCycliste> cSprint, liste de carte sprinteur
	 */
	private ArrayList<CarteCycliste> cSprint;
	/**
	 * Attribut ArrayList<CarteCycliste> defausseRoul, liste de defausse de carte rouleur
	 */
	private ArrayList<CarteCycliste> defausseRoul;
	/**
	 * Attribut ArrayList<CarteCycliste> defausseSprint, liste de defausse de carte sprinteur
	 */
	private ArrayList<CarteCycliste> defausseSprint;
	
	//CONSTRUCTEUR
	/**
	 * Constructeur de la classe Plateau, permettat d initialiser le plateau
	 * 
	 * @param c, couleur
	 */
	public Plateau(int c){
		
		this.defausseRoul = new ArrayList<CarteCycliste>();
		this.defausseSprint = new ArrayList<CarteCycliste>();
		this.cRoul=new ArrayList<CarteCycliste>();
		this.cSprint=new ArrayList<CarteCycliste>();
		this.couleur=c;
		
		for(int i=2 ; i<=5;i++){
			for(int j=0 ; j<3 ;j++)
				cSprint.add(new CarteSprinter(i,c));
		}
		for(int j=0 ; j<3 ;j++)
			cSprint.add(new CarteSprinter(9,c));
		
		for(int i=3 ; i<=7;i++){
			for(int j = 0 ; j<3 ;j++)
				cRoul.add(new CarteRouleur(i,c));
		}
		
	}
	
	//METHODES
	/**
	 * Methode void MelangerCarte, permettant de melanger la liste des cartes rouleur et sprinter
	 */
	public void MelangerCarte(){
		Collections.shuffle(cRoul);
		Collections.shuffle(cSprint);
		
	}
	
	/**
	 * Methode CarteRouleur piocheRouleur, permettant de piocher des cartes dans la liste rouleur
	 * 
	 * @return c
	 */
	public CarteRouleur piocheRouleur(){
		if(this.cRoul.size()==0){
			for(int i = 0 ; i<this.defausseRoul.size();i++){
				this.cRoul.add(this.defausseRoul.get(i));
			}
			this.defausseRoul.clear();
			Collections.shuffle(cRoul);
		}
		CarteRouleur c =(CarteRouleur)cRoul.get(0);
		cRoul.remove(0);
		return c;
	}
	
	/**
	 * Methode CarteSprinter piocheSprinter, permettant de piocher des cartes dans la liste sprinter
	 * 
	 * @return c
	 */
	public CarteSprinter piocheSprinter(){
		if(this.cSprint.size()==0){
			for(int i = 0 ; i<this.defausseSprint.size();i++){
				this.cSprint.add(this.defausseSprint.get(i));
			}
			this.defausseSprint.clear();
			Collections.shuffle(cSprint);
		}
		CarteSprinter c =(CarteSprinter)cSprint.get(0);
		cSprint.remove(0);
		return c;
	}
	
	/**
	 * Methode void defausseRouleur, permettant d ajouter les cartes rouleur en parametre dans la liste de defausse rouleur
	 * 
	 * @param cr, carte rouleur
	 */
	public void defausseRouleur(CarteRouleur cr){
		this.defausseRoul.add(cr);
	}
	
	/**
	 * Methode void defausseSprinter, permettant d ajouter les cartes sprinter en parametre dans la liste de defausse sprinter
	 * 
	 * @param cs, carte sprinter
	 */
	public void defausseSprinter(CarteSprinter cs){
		this.defausseSprint.add(cs);
	}
}
