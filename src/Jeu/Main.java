/**
 * Classe appartenant au package Jeu
 */
package Jeu;

/**
 * Importation de java.io.IOException
 */
import java.io.IOException;
import java.util.*;

/**
 * @author Mayer Theo, Hof Lucien
 */

/**
 * Classe Main, qui permet de lancer le jeu
 */
public class Main {
	
	/**
	 * Methode static void main, permettant de faire l affichage au depart et de lancer le jeu
	 * 
	 * @param args
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Avez vous un partie en cours ?(o) ou (n):");
		String save = sc.next();
		if(save.equals("n")){
			System.out.println("choisir une piste (entre 0 et 5)");
			int piste = sc.nextInt();
			System.out.println("choisir le nombre de joueur (entre 1 et 4)");
			int nbJoueur = sc.nextInt();
			Jeu j = new Jeu(piste,nbJoueur);
			System.out.println("__________");
			System.out.println("- - - - - ");
			System.out.println("__________");
			System.out.println("- - - - - ");
			System.out.println("__________");
			j.demarerjeu();
		}else{
			System.out.println("__________");
			System.out.println("- - - - - ");
			System.out.println("__________");
			System.out.println("- - - - - ");
			System.out.println("__________");
			Jeu j = new Jeu(0,1);
			Jeu vj= new Jeu(j.charger());
			vj.demarerjeu();
		}
		
	}

}
