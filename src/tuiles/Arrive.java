/**
 * Classe qui sont dans le package Tuiles
 */
package tuiles;

import java.io.Serializable;

/**
 * Importation du package Jeu
 */
import Jeu.*;

/**
 * @author Mayer Theo, Hof Lucien
 */

/**
 * Classe Arrive permettant de creer une tuile correspondant a l arrivee
 * Cette classe implemente Serializable
 */
public class Arrive extends Tuile implements Serializable{
	
	//CONSTRUCTEUR
	/**
	 * Constructeur de la classe Arrive, prenant en parametre une pente et initialisant l arrivee avec 5 cases
	 * 
	 * @param p, pente
	 */
	public Arrive(int p){
		super(p,5);
	}
	
	//METHODE
	/**
	 * Methode String toString permettant d'obtenir le nom de la tuile
	 * 
	 * @return A, pour Arrivee
	 */
	public String toString(){
		return "A";
	}
}
