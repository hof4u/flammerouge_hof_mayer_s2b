/**
 * Classe qui sont dans le package Tuiles
 */
package tuiles;

/**
 * @author Mayer Theo, Hof Lucien
 */

/**
 * Importation du package Jeu
 */
import Jeu.*;

/**
 * Classe VirageLarge permettant de creer un virage large 
 */
public class VirageLarge extends Tuile{
	
	//ATTRIBUT
	/**
	 * Attribut boolean direction correspondant a la direction du virage large
	 */
	private boolean direction;
	//direction = true : virage � droite
	//direction = flase : virage � gauche
	
	//CONSTRUCTEUR
	/**
	 * Constructeur de la classe VirageLarge, prenant en parametre une pente et initialisant la direction du virage
	 * 
	 * @param p, pente
	 * @param d, direction
	 */
	public VirageLarge(int p, boolean d){
		super(p,1);
		this.direction=d;
	}
	
	//METHODE
	/**
	 * Methode String toString permettant d'obtenir le nom de la tuile
	 * 
	 * @return t, pour LD ou pour LG
	 */
	public String toString(){
		String t="";
		if(this.direction==true)
			t="LD";
		else
			t="LG";
				
		return t;
	}
}
