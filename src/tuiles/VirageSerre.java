/**
 * Classe qui sont dans le package Tuiles
 */
package tuiles;

/**
 * Importation du package Jeu
 */
import Jeu.*;

/**
 * @author Mayer Theo, Hof Lucien
 */

/**
 * Classe VirageSerre permettant de creer un virage serre 
 */
public class VirageSerre extends Tuile{
	
	//ATTRIBUT
	/**
	 * Attribut boolean direction correspondant a la direction du virage serre
	 */
	private boolean direction;
	//direction = true : virage � droite
	//direction = false : virage � gauche
	
	//CONSTRUCTEUR
	/**
	 * Constructeur de la classe VirageLarge, prenant en parametre une pente et initialisant la direction du virage
	 * 
	 * @param p, pente
	 * @param d, direction
	 */
	public VirageSerre (int p, boolean d){
		super(p,1);
		this.direction=d;
	}
	
	//METHODE
	/**
	 * Methode String toString permettant d'obtenir le nom de la tuile
	 * 
	 * @return t, pour SD ou pour SG
	 */
	public String toString(){
		String t="";
		if(this.direction==true)
			t="SD";
		else
			t="SG";
				
		return t;
	}
}
