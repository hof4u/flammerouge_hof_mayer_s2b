/**
 * Classe qui sont dans le package Tuiles
 */
package tuiles;

import java.io.Serializable;

/**
 * Importation du package Jeu
 */
import Jeu.*;

/**
 * @author Mayer Theo, Hof Lucien
 */

/**
 * Classe Tuile permettant de creer une tuile
 * Cette classe implemente Serializable 
 */
public abstract class Tuile implements Serializable{
	
	//ATTRIBUTS
	/**
	 * Attribut int pente correspondant a la pente de la tuile
	 */
	private int pente;
	/**
	 * Attribut int cases correspondant aux nombres de cases sur la tuile
	 */
	private int cases;
	
	//CONSTRUCTEUR
	/**
	 * Constructeur de la classe Tuile, prenant en parametre une pente et un nombre de cases sur la tuile
	 * 
	 * @param p, pente
	 */
	public Tuile(int p,int nbC){
		this.pente=p;
		this.cases=nbC;
	}
	
	//METHODE
	/**
	 * Methode String toString permettant d'obtenir le nom de la tuile
	 * 
	 * @return "", vide car le nom de la tuile est initialise avec les autres classes
	 */
	public String toString(){
		return "";
	}
	
	/**
	 * Methode int getTaille permettant de retourner le nombres de cases de la tuile
	 * 
	 * @return this.cases
	 */
	public int getTaille(){
		return this.cases;
	}
	
	public int getPenre(){
		return this.pente;
	}

}
