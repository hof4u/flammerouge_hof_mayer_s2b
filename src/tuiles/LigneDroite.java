/**
 * Classe qui sont dans le package Tuiles
 */
package tuiles;

import java.io.Serializable;

/**
 * Importation du package Jeu
 */
import Jeu.*;

/**
 * @author Mayer Theo, Hof Lucien
 */

/**
 * Classe LigneDroite permettant de creer une tuile correspondant a une ligne droite
 * Cette classe implemente Serializable
 */
public class LigneDroite extends Tuile implements Serializable{
	
	//CONSTRUCTEUR
	/**
	 * Constructeur de la classe LigneDroite, prenant en parametre une pente et initialisant la ligne droite avec 5 cases
	 * 
	 * @param p, pente
	 */
	public LigneDroite(int p){
		super(p,5);
	}
	
	//METHODE
	/**
	 * Methode String toString permettant d'obtenir le nom de la tuile
	 * 
	 * @return L, pour Ligne Droite
	 */
	public String toString(){
		return "L";
	}
}
