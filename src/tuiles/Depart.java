/**
 * Classe qui sont dans le package Tuiles
 */
package tuiles;

import java.io.Serializable;

/**
 * Importation du package Jeu
 */
import Jeu.*;

/**
 * @author Mayer Theo, Hof Lucien
 */

/**
 * Classe Depart permettant de creer une tuile correspondant au depart
 * Cette classe implemente Serializable
 */
public class Depart extends Tuile implements Serializable{
	
	//CONSTRUCTEUR
	/**
	 * Constructeur de la classe Depart, prenant en parametre une pente et initialisant le depart avec 5 cases
	 * 
	 * @param p, pente
	 */
	public Depart(int p){
		super(p,5);
	}
	
	//METHODE
	/**
	 * Methode String toString permettant d'obtenir le nom de la tuile
	 * 
	 * @return D, pour Depart
	 */
	public String toString(){
		return "D";
	}
}
