/**
 * Classe qui sont dans le package Carte
 */
package Carte;

import java.io.Serializable;

/**
 * @author Mayer Theo, Hof Lucien
 */

/*
 * Classe CarteFatigue, cela correspond aux cartes fatigues en general
 * Cette classe implemente Serializable
 */
public abstract class CarteFatigue implements Serializable{
	
	//ATTRIBUT
	/**
	 * Attributs int valCarte, correspondant a la valeur de la carte fatigue
	 */
	private int valCarte;
	
	//CONSTRUCTEUR
	/**
	 * Construction de la classe CarteFatigue
	 */
	public CarteFatigue (){
		this.valCarte=2;
	}
	
	
	//METHODES
	/**
	 * Methode String toString permettant d'obtenir le nom de la carte
	 * 
	 * @return Carte Fatigue
	 */
	public String toString(){
		return ("Carte Fatigue ");
	}
	
}
