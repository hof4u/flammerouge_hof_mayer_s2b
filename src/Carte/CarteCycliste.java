/**
 * Classe qui sont dans le package Carte
 */
package Carte;

import java.io.Serializable;

/**
 * @author Mayer Theo, Hof Lucien
 */

/*
 * Classe CarteCycliste, cela correspond aux cartes des cyclistes en general
 * Cette classe implemente Serializable
 */
public abstract class CarteCycliste implements Serializable{
	
	//ATTRIBUTS
	/**
	 * Attributs int Couleur, correspond a la couleur de la carte, 1=rouge, 2=verte, 3=bleue, 4=noire
	 */
	private int Couleur; 
	/**
	 * Attributs int valCarte, correspondant a la valeur de la carte cycliste
	 */
	private int valCarte;
	
	//CONSTRUCTEUR
	/**
	 * Construction de la classe CarteCycliste
	 * 
	 * @param int v, attribut correspondant a la valeur de la carte Cycliste
	 * @param int c, attribut correspondant a la couleur de la carte Cycliste
	 */
	public CarteCycliste(int v, int c){
		this.valCarte=v;
		this.Couleur=c;
	}
	
	//METHODES
	/**
	 * Methode int getValeur permettant d obtenir la valeur de la carte
	 *  
	 * @return this.valCarte, valeur de la carte cycliste
	 */
	public int getValeur(){
		return this.valCarte;
	}
	
	/**
	 * Methode String toString permettant d'obtenir le nom de la carte
	 * 
	 * @return Carte Cycliste
	 */
	public String toString(){
		return ("Carte Cycliste ");
	}
}
