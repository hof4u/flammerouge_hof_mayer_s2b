/**
 * Classe qui sont dans le package Carte
 */
package Carte;

/**
 * @author Mayer Theo, Hof Lucien
 */

/*
 * Classe Sprinter, cela correspond aux cartes fatigue des sprinters
 */
public class Sprinter extends CarteFatigue{
		
		//CONSTRUCTEUR
		/**
		 * Construction de la classe Sprinter
		 */
		public Sprinter (){
			super();
		}
		
		//METHODES
		/**
		 * Methode String toString, permettant de retourner le nom de la carte
		 * 
		 * @return super.toString() + "Sprinter", ce qui donner Carte Fatigue Sprinter
		 */
		public String toString(){
			return (super.toString() + "Sprinter");
		}
}
