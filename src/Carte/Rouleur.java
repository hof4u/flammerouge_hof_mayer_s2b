/**
 * Classe qui sont dans le package Carte
 */
package Carte;

/**
 * @author Mayer Theo, Hof Lucien
 */

/*
 * Classe Rouleur, cela correspond aux cartes fatigue des rouleurs
 */
public class Rouleur extends CarteFatigue{
	
	//CONSTRUCTEUR
	/**
	 * Construction de la classe Rouleur
	 */
	public Rouleur(){
		super();
	}
	
	//METHODES
	/**
	 * Methode String toString, permettant de retourner le nom de la carte
	 * 
	 * @return super.toString() + "Rouleur", ce qui donner Carte Fatigue Rouleur
	 */
	public String toString(){
		return (super.toString() + "Rouleur");
	}
}
