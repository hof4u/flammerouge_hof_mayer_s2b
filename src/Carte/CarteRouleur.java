/**
 * Classe qui sont dans le package Carte
 */
package Carte;

/**
 * @author Mayer Theo, Hof Lucien
 */

/*
 * Classe CarteRouleur, cela correspond aux cartes des rouleurs
 */
public class CarteRouleur extends CarteCycliste{
	
	//CONSTRUCTEUR
	/**
	 * Construction de la classe CarteRouleur
	 * 
	 * @param int v, valeur de la carte rouleur
	 * @param int c, couleur de la rouleur, varie selon le numero donn�
	 */
	public CarteRouleur(int v, int c){
		super(v,c);
	}
	
	//METHODES
	/**
	 * Methode String toString, permettant de retourner le nom de la carte rouleur
	 * 
	 * @return super.toString() + "Rouleur de valeur :" +super.getValeur(), ce qui donnera Carte Cycliste Rouleur de valeur ... 
	 */
	public String toString(){
		return (super.toString() + "Rouleur de valeur :" +super.getValeur());
	}
}
