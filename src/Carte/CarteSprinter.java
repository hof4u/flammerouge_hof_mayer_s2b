/**
 * Classe qui sont dans le package Carte
 */
package Carte;

/**
 * @author Mayer Theo, Hof Lucien
 */

/*
 * Classe CarteSprinter, cela correspond aux cartes des sprinters
 */
public class CarteSprinter extends CarteCycliste{
	
	//CONSTRUCTEUR
	/**
	 * Construction de la classe CarteSprinter
	 * 
	 * @param int v, valeur de la carte sprinter
	 * @param int c, couleur de la sprinter, varie selon le numero donn�
	 */
	public CarteSprinter(int v, int c){
		super(v,c);
	}
	
	//METHODES
	/**
	 * Methode String toString, permettant de retourner le nom de la carte sprinter
	 * 
	 * @return super.toString() + "Sprinter de valeur :" +super.getValeur(), ce qui donnera Carte Cycliste Sprinter de valeur ... 
	 */
	public String toString(){
		return (super.toString() + "Sprinter de valeur :" +super.getValeur());
	}
}
